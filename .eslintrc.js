module.exports = {
    env: {
        es6: true,
        node: true,
        mocha: true,
    },
    extends: ['airbnb-base'],
    parserOptions: {
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    rules: {
        indent: ['error', 4],
        'class-methods-use-this': 'off',
        // 'no-param-reassign': 'off',
        // camelcase: 'off',
        'no-unused-vars': ['error', { argsIgnorePattern: 'next' }],
    },
};
