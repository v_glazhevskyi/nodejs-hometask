const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { NotFoundError, HandlingError } = require('../errors');

const router = Router();

router.get('/', (req, res, next) => {
    const data = UserService.getAll();
    if (!data) {
        throw new NotFoundError('No any user found');
    }
    res.data = data;
    next();
});

router.get('/:id', (req, res, next) => {
    const { id } = req.params;
    const data = UserService.getById(id);
    if (!data) {
        throw new NotFoundError('User not found');
    }
    res.data = data;
    next();
});

router.post('/', createUserValid, (req, res, next) => {
    const user = req.body;
    const data = UserService.create(user);
    if (!data) {
        throw new HandlingError('User not created');
    }
    res.data = data;
    next();
});

router.put('/:id', updateUserValid, (req, res, next) => {
    const { id } = req.params;
    const user = req.body;
    const data = UserService.updateById(id, user);
    if (!data) {
        throw new HandlingError('No user to update');
    }
    res.data = data;
    next();
});

router.delete('/:id', (req, res, next) => {
    const { id } = req.params;
    const success = UserService.deleteById(id);
    if (!success) {
        throw new HandlingError('No user to delete');
    }
    res.data = {};
    next();
});

module.exports = router;
