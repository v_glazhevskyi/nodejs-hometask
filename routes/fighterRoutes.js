const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const { NotFoundError, HandlingError } = require('../errors');

const router = Router();

router.get('/', (req, res, next) => {
    const data = FighterService.getAll();
    if (!data) {
        throw new NotFoundError('No any fighter found');
    }
    res.data = data;
    next();
});

router.get('/:id', (req, res, next) => {
    const { id } = req.params;
    const data = FighterService.getById(id);
    if (!data) {
        throw new NotFoundError('Fighter not found');
    }
    res.data = data;
    next();
});

router.post('/', createFighterValid, (req, res, next) => {
    const fighter = req.body;
    const data = FighterService.create(fighter);
    if (!data) {
        throw new HandlingError('Fighter not created');
    }
    res.data = data;
    next();
});

router.put('/:id', updateFighterValid, (req, res, next) => {
    const { id } = req.params;
    const fighter = req.body;
    const data = FighterService.updateById(id, fighter);
    if (!data) {
        throw new HandlingError('No fighter to update');
    }
    res.data = data;
    next();
});

router.delete('/:id', (req, res, next) => {
    const { id } = req.params;
    const success = FighterService.deleteById(id);
    if (!success) {
        throw new HandlingError('No fighter to delete');
    }
    res.data = {};
    next();
});

module.exports = router;
