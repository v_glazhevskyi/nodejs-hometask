const { Router } = require('express');
const AuthService = require('../services/authService');

const router = Router();

router.post('/login', (req, res, next) => {
    const userData = req.body;
    const data = AuthService.login(userData);
    res.data = data;
    next();
});

module.exports = router;
