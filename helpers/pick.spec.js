/* eslint-disable no-unused-expressions */
const { assert, expect } = require('chai');
const pick = require('./pick');

describe('Test pick', () => {
    it('Edge cases', () => {
        assert.doesNotThrow(() => pick());
        assert.doesNotThrow(() => pick('a'));

        expect(pick()).to.be.an('object').that.is.empty;
        expect(pick([], {})).to.be.an('object').that.is.empty;
        expect(pick(['a'], {})).to.be.an('object').that.is.empty;
        expect(pick([], { a: 1 })).to.be.an('object').that.is.empty;
    });

    it('Should work', () => {
        const obj = { a: 1, b: 2, c: 3 };
        const result = pick(['c', 'a'], obj);
        expect(result).not.to.have.key('b');
        expect(result).to.have.keys(['a', 'c']);
    });
});
