const { assert } = require('chai');
const isSubset = require('./isSubset');

describe('Test isSubset', () => {
    it('Should be truthy', () => {
        assert.isTrue(isSubset([], []));
        assert.isTrue(isSubset([], [1]));
        assert.isTrue(isSubset([1], [1]));
        assert.isTrue(isSubset([1], [1, 2]));
        assert.isTrue(isSubset([1, 3], [3, 2, 1]));
    });

    it('Should be truthy too', () => {
        assert.isTrue(isSubset([1, 1], [1]));
        assert.isTrue(isSubset([1, 2, 2, 1], [1, 2]));
    });

    it('Should be falsy', () => {
        assert.isFalse(isSubset([1], []));
        assert.isFalse(isSubset([1], [2]));
        assert.isFalse(isSubset([1, 2], [1]));
        assert.isFalse(isSubset([1, 2, 3], [3, 2, 2]));
    });
});
