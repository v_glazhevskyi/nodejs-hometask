const isSubset = require('./isSubset');

/**
 * Comparing two arrays. Order of elements isn't important.
 * @param {Array} listA
 * @param {Array} listB
 */
const isEqualArrays = (listA, listB) => {
    if (listA.length !== listB.length) {
        return false;
    }
    return isSubset(listA, listB) && isSubset(listB, listA);
};

module.exports = isEqualArrays;
