const { assert } = require('chai');
const omit = require('./omit');

describe('Test omit', () => {
    it('Edge cases', () => {
        assert.doesNotThrow(() => omit());
        assert.doesNotThrow(() => omit('a'));

        assert.isObject(omit());
        assert.isEmpty(omit());

        assert.isObject(omit([], {}));
        assert.isEmpty(omit([], {}));
        assert.isEmpty(omit(['a'], {}), {});
        assert.containsAllKeys(omit([], { a: 1 }), ['a']);
    });

    it('Should work', () => {
        const obj = { a: 1, b: 2, c: 3 };
        assert.containsAllKeys(omit(['c', 'a'], obj), ['b']);
    });

    it('Should work too', () => {
        const obj = { a: 1, b: 2, c: 3 };
        assert.containsAllKeys(omit('b', obj), ['a', 'c']);
    });
});
