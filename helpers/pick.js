/**
 * Returns a partial copy of an object containing only the keys specified.
 * If the key does not exist, the property is ignored.
 * @param {Array | String} keys Keys to pick
 * @param {Object} obj
 */
const pick = (keys, obj = {}) => Object.keys(obj).reduce((acc, key) => {
    if (keys.includes(key)) {
        return { ...acc, [key]: obj[key] };
    }
    return acc;
}, {});

module.exports = pick;
