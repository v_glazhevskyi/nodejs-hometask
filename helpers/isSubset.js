const isSubset = (subset, set) => subset.every((v) => set.includes(v));

module.exports = isSubset;
