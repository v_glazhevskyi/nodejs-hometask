const { assert } = require('chai');
const isEqualArrays = require('./isEqualArrays');

describe('Test isEqualArrays', () => {
    it('Should be truthy', () => {
        assert.isTrue(isEqualArrays([], []));
        assert.isTrue(isEqualArrays([1], [1]));
        assert.isTrue(isEqualArrays([1, 2], [1, 2]));
        assert.isTrue(isEqualArrays([1, 2], [2, 1]));
    });

    it('Should be falsy', () => {
        assert.isFalse(isEqualArrays([1], []));
        assert.isFalse(isEqualArrays([], [1]));
        assert.isFalse(isEqualArrays([1], [2]));
        assert.isFalse(isEqualArrays([1], [1, 2]));
        assert.isFalse(isEqualArrays([1, 2], [1]));
    });
});
