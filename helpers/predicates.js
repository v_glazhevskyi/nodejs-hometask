const isTrue = (value) => !!value;
const isFalse = (value) => !value;
const excludes = (list) => (item) => !list.includes(item);
const includes = (list) => (item) => list.includes(item);

exports.isTrue = isTrue;
exports.isFalse = isFalse;
exports.excludes = excludes;
exports.includes = includes;
