const isString = (s) => typeof s === 'string';
const isNumber = (n) => typeof n === 'number';

exports.isString = isString;
exports.isNumber = isNumber;
