/**
 * Returns a partial copy of an object omitting the keys specified.
 * @param {Array | String} keys Keys to drop
 * @param {Object} obj
 */
const omit = (keys, obj = {}) => Object.keys(obj).reduce((acc, key) => {
    if (keys.includes(key)) {
        return acc;
    }
    return { ...acc, [key]: obj[key] };
}, {});

module.exports = omit;
