const isSubset = require('./isSubset');
const isEqualArrays = require('./isEqualArrays');
exports.isSubset = isSubset;
exports.isEqualArrays = isEqualArrays;
exports.eachOf = (b) => (a) => isEqualArrays(a, b);
exports.someOf = (b) => (a) => isSubset(a, b);

exports.omit = require('./omit');
exports.pick = require('./pick');

const typeChecking = require('./typeChecking');
exports.isString = typeChecking.isString;
exports.isNumber = typeChecking.isNumber;

const predicates = require('./predicates');
exports.isTrue = predicates.isTrue;
exports.isFalse = predicates.isFalse;
exports.includes = predicates.includes;
exports.excludes = predicates.excludes;

