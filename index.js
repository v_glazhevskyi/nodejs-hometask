const express = require('express');
const cors = require('cors');
require('./config/db');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const { responseMiddleware } = require('./middlewares/response.middleware');
const { handleErrorsMiddleware } = require('./middlewares/handleErrors.middleware');

const routes = require('./routes/index');

routes(app);

app.use('/', express.static('./client/build'));

app.use(responseMiddleware);
app.use(handleErrorsMiddleware);

const port = 3050;
app.listen(port, () => {});

exports.app = app;
