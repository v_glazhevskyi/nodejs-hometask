class ValidationError extends Error {
    toString() {
        return this.message;
    }
}

module.exports = ValidationError;
