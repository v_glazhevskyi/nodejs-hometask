class HandlingError extends Error {
    toString() {
        return this.message;
    }
}

module.exports = HandlingError;
