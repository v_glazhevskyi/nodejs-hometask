class NotFoundError extends Error {
    toString() {
        return this.message;
    }
}

module.exports = NotFoundError;
