const { UserRepository } = require('../repositories/userRepository');

class UserService {
    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    getById(id) {
        return this.search({ id });
    }

    getAll() {
        const items = UserRepository.getAll();
        if (!items.length) {
            return null;
        }
        return items;
    }

    create(user) {
        const { email } = user;
        const exist = this.search({ email });
        if (exist) {
            return null;
        }
        const item = UserRepository.create(user);
        if (!item) {
            return null;
        }
        return item;
    }

    updateById(id, user) {
        const exist = this.getById(id);
        if (!exist) {
            return null;
        }
        const item = UserRepository.update(id, user);
        if (!item) {
            return null;
        }
        return item;
    }

    deleteById(id) {
        const items = UserRepository.delete(id);
        return !!items.length;
    }
}

module.exports = new UserService();
