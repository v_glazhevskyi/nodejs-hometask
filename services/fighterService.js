const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    getById(id) {
        return this.search({ id });
    }

    getAll() {
        const items = FighterRepository.getAll();
        if (!items.length) {
            return null;
        }
        return items;
    }

    create(fighter) {
        const { name } = fighter;
        const exist = this.search({ name });
        if (exist) {
            return null;
        }
        const item = FighterRepository.create(fighter);
        if (!item) {
            return null;
        }
        return item;
    }

    updateById(id, fighter) {
        const exist = this.getById(id);
        if (!exist) {
            return null;
        }
        // Restrict to rename fighter, if another fighter is exist with same name
        const { name } = fighter;
        const another = this.search({ name });
        if (another && another.id !== id) {
            return null;
        }
        const item = FighterRepository.update(id, fighter);
        if (!item) {
            return null;
        }
        return item;
    }

    deleteById(id) {
        const items = FighterRepository.delete(id);
        return !!items.length;
    }
}

module.exports = new FighterService();
