const { BaseRepository } = require('./baseRepository');

class FighterRepository extends BaseRepository {
    constructor() {
        super('fighters');
    }

    create(data) {
        return super.create({ ...data, health: 100 });
    }
}

exports.FighterRepository = new FighterRepository();
