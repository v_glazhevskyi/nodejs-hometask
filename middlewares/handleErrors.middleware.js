const { NotFoundError, HandlingError, ValidationError } = require('../errors');

const getStatusCodeByError = (err) => {
    if (err instanceof NotFoundError) {
        return 404;
    }
    if (err instanceof HandlingError || err instanceof ValidationError) {
        return 400;
    }
    return 500;
};

const handleErrorsMiddleware = (err, req, res, next) => {
    if (res.headersSent) {
        return next(err);
    }
    const code = getStatusCodeByError(err);
    res.status(code).json({ error: true, message: String(err) });
};

exports.handleErrorsMiddleware = handleErrorsMiddleware;
