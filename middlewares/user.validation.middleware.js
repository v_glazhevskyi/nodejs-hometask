const { createValidationMiddleware } = require('./validation/utils');
const { validateNewUser, validateExistUser } = require('./validation/validateUserEntity');

const createUserValid = createValidationMiddleware(validateNewUser, 'User entity to create isn\'t valid');
const updateUserValid = createValidationMiddleware(validateExistUser, 'User entity to update isn\'t valid');

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
