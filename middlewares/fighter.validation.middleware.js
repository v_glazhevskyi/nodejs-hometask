const { createValidationMiddleware } = require('./validation/utils');
const { validateNewFighter, validateExistFighter } = require('./validation/validateFighterEntity');

const createFighterValid = createValidationMiddleware(validateNewFighter, 'Fighter entity to create isn\'t valid');
const updateFighterValid = createValidationMiddleware(validateExistFighter, 'Fighter entity to update isn\'t valid');

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
