const { isString, isNumber } = require('../../helpers');

const isValidName = (username) => {
    const regex = /^[a-zA-Z-]+$/;
    return isString(username) && regex.test(username);
};

const isValidEmail = (email) => {
    const regex = /^[a-z0-9._]+@gmail\.com$/;
    return isString(email) && regex.test(email.toLowerCase());
};

const isValidPhone = (phone) => {
    const regex = /^(\+380)\d{9}$/;
    return isString(phone) && regex.test(phone);
};

const isValidPwd = (pwd) => {
    const regex = /^[0-9a-zA-Z!@#$%^&*]{3,}$/;
    return isString(pwd) && regex.test(pwd);
};

const isValidPower = (value) => isNumber(value) && value > 0 && value < 100;

const isValidDefence = (value) => isNumber(value) && value > 0 && value < 11;

const isValidHealth = (value) => isNumber(value) && value >= 0 && value <= 100;

exports.isValidName = isValidName;
exports.isValidEmail = isValidEmail;
exports.isValidPhone = isValidPhone;
exports.isValidPwd = isValidPwd;
exports.isValidPower = isValidPower;
exports.isValidDefence = isValidDefence;
exports.isValidHealth = isValidHealth;
