const { ValidationError } = require('../../errors');

const createValidationMiddleware = (validationFn, errMessage) => (req, res, next) => {
    if (validationFn(req.body)) {
        return next();
    }
    throw new ValidationError(errMessage);
};

const createEntityValidator = (compareKeys, validateKeys) => (data) => {
    const keys = Object.keys(data);
    const validate = validateKeys(keys);
    return compareKeys(keys) && validate(data);
};

const validationDispatcher = (fnByKey) => (keysToValidate) => (data) => keysToValidate.every(
    (key) => {
        const value = data[key];
        const isValid = fnByKey[key];
        return isValid(value);
    },
);

exports.createValidationMiddleware = createValidationMiddleware;
exports.createEntityValidator = createEntityValidator;
exports.validationDispatcher = validationDispatcher;
