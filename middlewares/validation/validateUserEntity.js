const { user } = require('../../models/user');
const { excludes, eachOf, someOf } = require('../../helpers');
const { isValidName, isValidPhone, isValidEmail, isValidPwd } = require('./primitives');
const { validationDispatcher, createEntityValidator } = require('./utils');

const requiredKeys = Object.keys(user).filter(excludes('id'));

const validateKeys = validationDispatcher({
    firstName: isValidName,
    lastName: isValidName,
    email: isValidEmail,
    phoneNumber: isValidPhone,
    password: isValidPwd,
});

exports.validateNewUser = createEntityValidator(eachOf(requiredKeys), validateKeys);
exports.validateExistUser = createEntityValidator(someOf(requiredKeys), validateKeys);
