const { fighter } = require('../../models/fighter');
const { excludes, eachOf, someOf } = require('../../helpers');
const { isValidName, isValidHealth, isValidPower, isValidDefence } = require('./primitives');
const { validationDispatcher, createEntityValidator } = require('./utils');

const validateKeys = validationDispatcher({
    name: isValidName,
    health: isValidHealth,
    power: isValidPower,
    defense: isValidDefence,
});

const requiredKeys = Object.keys(fighter).filter(
    excludes(['id', 'health']),
);

const keysAllowedToUpdate = Object.keys(fighter).filter(
    excludes('id'),
);

exports.validateNewFighter = createEntityValidator(eachOf(requiredKeys), validateKeys);
exports.validateExistFighter = createEntityValidator(someOf(keysAllowedToUpdate), validateKeys);
