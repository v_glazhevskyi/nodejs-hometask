/* eslint-disable no-unused-expressions */
/* eslint-disable no-restricted-syntax */
const { assert } = require('chai');
const { validateNewFighter } = require('./validateFighterEntity');
const { omit } = require('../../helpers');

const fighter = {
    name: 'Scorpio',
    power: 45, // 1 to 100
    defense: 8, // 1 to 10
};

describe('Test fighter entity validation', () => {
    it('Should be true', () => {
        assert.isTrue(validateNewFighter(fighter));
    });

    it('Shouldn\'t contains id', () => {
        assert.isFalse(validateNewFighter({ id: 'abc', ...fighter }));
    });

    it('Should have all required field', () => {
        const required = ['name', 'power', 'defense'];

        // Each entity lack one required field.
        const invalidEntities = required.map((key) => omit([key], fighter));

        for (const entity of invalidEntities) {
            assert.isFalse(validateNewFighter(entity));
        }
    });

    it('Should validate power', () => {
        assert.isFalse(validateNewFighter({ ...fighter, power: -1 }));
        assert.isFalse(validateNewFighter({ ...fighter, power: 0 }));
        assert.isTrue(validateNewFighter({ ...fighter, power: 1 }));
        assert.isTrue(validateNewFighter({ ...fighter, power: 99 }));
        assert.isFalse(validateNewFighter({ ...fighter, power: 100 }));
    });

    it('Should validate defense', () => {
        assert.isFalse(validateNewFighter({ ...fighter, defense: -1 }));
        assert.isFalse(validateNewFighter({ ...fighter, defense: 0 }));
        assert.isTrue(validateNewFighter({ ...fighter, defense: 1 }));
        assert.isTrue(validateNewFighter({ ...fighter, defense: 10 }));
        assert.isFalse(validateNewFighter({ ...fighter, defense: 11 }));
    });
});
