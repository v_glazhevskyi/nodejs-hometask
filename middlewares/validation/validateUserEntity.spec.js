/* eslint-disable no-restricted-syntax */
const { assert } = require('chai');
const { validateNewUser } = require('./validateUserEntity');
const { omit } = require('../../helpers');

const user = {
    firstName: 'John',
    lastName: 'Connor',
    email: 'john.c@gmail.com',
    phoneNumber: '+380631234567',
    password: 'secret', // min 3 symbols
};

describe('Test user entity validation', () => {
    it('Should be true', () => {
        assert.isTrue(validateNewUser(user));
    });

    it('Shouldn\'t contains id', () => {
        assert.isFalse(validateNewUser({ id: 'abc', ...user }));
    });

    it('Should have all required field', () => {
        const required = [
            'firstName',
            'lastName',
            'email',
            'phoneNumber',
            'password',
        ];

        // Each entity lack one required field.
        const invalidEntities = required.map((key) => omit([key], user));

        for (const entity of invalidEntities) {
            assert.isFalse(validateNewUser(entity));
        }
    });

    it('Should be falsy on invalid phone number', () => {
        const invalidPhoneNumbers = [null, undefined, '', '12345', '0631234567', '380631234567', '+3806312345', '+3806312345678', '+3806312345abc'];

        const invalidEntities = invalidPhoneNumbers.map(
            (phoneNumber) => ({ ...user, phoneNumber }),
        );

        for (const entity of invalidEntities) {
            assert.isFalse(validateNewUser(entity));
        }
    });

    it('Should be falsy on invalid email', () => {
        const invalidEmails = [null, undefined, '', 'test@com', 'test@example.com', 'test@ukr.net'];

        const invalidEntities = invalidEmails.map(
            (email) => ({ ...user, email }),
        );

        for (const entity of invalidEntities) {
            assert.isFalse(validateNewUser(entity));
        }
    });

    it('Should be falsy on invalid password', () => {
        const invalidPasswords = [null, undefined, '', 'aa'];

        const invalidEntities = invalidPasswords.map(
            (password) => ({ ...user, password }),
        );

        for (const entity of invalidEntities) {
            assert.isFalse(validateNewUser(entity));
        }
    });
});
