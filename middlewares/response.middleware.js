const { HandlingError } = require("../errors");

const responseMiddleware = (req, res, next) => {
    if (!res.data) {
        const err = new HandlingError(`Cannot ${req.method} ${req.path}`);
        return next(err);
    }
    res.status(200).json(res.data);
    next();
};

exports.responseMiddleware = responseMiddleware;
